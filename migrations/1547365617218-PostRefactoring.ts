import {MigrationInterface, QueryRunner} from 'typeorm';

export class PostRefactoring1547365617218 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP TABLE Cat`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE Cat`);
    }

}
