module.exports = {
    "type": "mysql",
    "host": process.env.TYPEORM_HOST || "localhost",
    "port": process.env.TYPEORM_PORT || 3308,
    "username": process.env.TYPEORM_USERNAME || "root",
    "password": process.env.TYPEORM_PASSWORD || "root",
    "database": process.env.TYPEORM_DATABASE || "QueueManagerDB",
    "entities": ["src/**/**.entity{.ts,.js}", "dist/**/**.entity.js"],
    "migrationsTableName": "migrations_table",
    "migrations": [
        "migrations/**/*.ts"
    ],
    "cli": {
        "migrationsDir": "migrations"
    },
    "synchronize": true
};
