FROM node:10

MAINTAINER yaroslav.velianyk@gmail.com

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN npm install
RUN npm install typeorm -g

COPY . /usr/src/app

EXPOSE $PORT

CMD ["npm", "run", "start:prod"]
